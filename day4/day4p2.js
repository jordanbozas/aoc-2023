const { processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day4/input");
  let totalCards = [];

  entries.map((entry) => {
    const cards = getScratchCards(entry);
    totalCards = totalCards.concat(cards);
  });

  console.log("🚀 ~ file: day4p2.js:7 ~ runPuzzle ~ totalCards:", totalCards)

  // console.log(sum);
  // answer = 27059
}

function getScratchCards(entry) {
  const gameNumber = Number(entry.split(': ')[0].split(' ')[1]);
  const winningNumbers = _.compact(entry.split(': ')[1].split(' | ')[0].split(' '));
  const scratchNumbers = _.compact(entry.split(': ')[1].split(' | ')[1].split(' '));
  const matchingNumbers = _.intersection(winningNumbers, scratchNumbers);
  
  const cards = [];

  cards.push(gameNumber);

  for (let x = 1; x <= matchingNumbers.length; x++) {
    cards.push(gameNumber + x);
  }
  
  return cards;
}

module.exports = { runPuzzle };
