const { processLineByLine } = require("../helpers");
const _ = require("lodash");
const digits = [0,1,2,3,4,5,6,7,8,9];
const numberMap = [{
  name: 'one',
  value: 1
}, {
  name: 'two',
  value: 2
}, {
  name: 'three',
  value: 3
}, {
  name: 'four',
  value: 4
}, {
  name: 'five',
  value: 5
}, {
  name: 'six',
  value: 6
}, {
  name: 'seven',
  value: 7
}, {
  name: 'eight',
  value: 8
}, {
  name: 'nine',
  value: 9
}];

async function runPuzzle() {
  const entries = await processLineByLine("./day1/input");
  let sum = 0;

  entries.map((entry) => {
    const value = getCalibrationValue(entry);
    sum += Number(value);
  });

  console.log(sum);
  // answer = 53515
}

function getCalibrationValue(entry) {
  const firstNumber = getFirstNumber(entry);
  const lastNumber = getLastNumber(entry);
  return Number(`${firstNumber}${lastNumber}`);
}

function getFirstNumber(entry) {
  let index = entry.length;
  let number = 0;

  numberMap.forEach(element => {
    let foundIndex = entry.indexOf(element.name);
    if (foundIndex >= 0 && foundIndex < index) {
      index = foundIndex;
      number = element.value;
    }
  });

  digits.forEach(element => {
    let foundIndex = entry.indexOf(element);
    if (foundIndex >= 0 && foundIndex < index) {
      index = foundIndex;
      number = element;
    }
  });

  return number;
}

function getLastNumber(entry) {
  let index = -1;
  let number = 0;

  numberMap.forEach(element => {
    let foundIndex = entry.lastIndexOf(element.name);
    if (foundIndex >= 0 && foundIndex > index) {
      index = foundIndex;
      number = element.value;
    }
  });

  digits.forEach(element => {
    let foundIndex = entry.lastIndexOf(element);
    if (foundIndex >= 0 && foundIndex > index) {
      index = foundIndex;
      number = element;
    }
  });

  return number;
}

module.exports = { runPuzzle };
