const { processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day1/input");
  let sum = 0;

  entries.map((entry) => {
    const value = getCalibrationValue(entry);
    sum += Number(value);
  });

  console.log(sum);
  // answer = 54388
}

function getCalibrationValue(entry) {
  const numbers = entry.replace(/\D/g, "");
  const firstNumber = Number(numbers[0]);
  const lastNumber = Number(numbers[numbers.length - 1]);

  return Number(`${firstNumber}${lastNumber}`);
}

module.exports = { runPuzzle };
