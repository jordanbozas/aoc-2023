const { processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day2/input");
  let sum = 0;

  entries.map((entry) => {
    const value = getValidGame(entry);
    sum += Number(value);
  });

  console.log(sum);
  // answer = 2720
}

function getValidGame(entry) {
  const gameNumber = Number(entry.split(':')[0].split(' ')[1]);
  const sets = entry.split(':')[1].split(';');
  const cubes = sets.map(set => {
    const parts = set.split(',').map(rawPart => {
      const part = rawPart.slice(1);
      return {
        color: part.split(' ')[1],
        count: Number(part.split(' ')[0])
      };
    });
    return {
      blue: _.sumBy(_.filter(parts, { color: 'blue'}), 'count'),
      red: _.sumBy(_.filter(parts, { color: 'red'}), 'count'),
      green: _.sumBy(_.filter(parts, { color: 'green'}), 'count'),
    };
  })
  const invalidRed = _.find(cubes, cube => cube.red > 12);
  const invalidGreen = _.find(cubes, cube => cube.green > 13);
  const invalidBlue = _.find(cubes, cube => cube.blue > 14);

  if (invalidRed || invalidGreen || invalidBlue) {
    return 0;
  }
  
  return gameNumber;
}

module.exports = { runPuzzle };
