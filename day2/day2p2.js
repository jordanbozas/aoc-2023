const { processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day2/input");
  let sum = 0;

  entries.map((entry) => {
    const value = getValidGame(entry);
    sum += Number(value);
  });

  console.log(sum);
  // answer = 71535
}

function getValidGame(entry) {
  const sets = entry.split(':')[1].split(';');
  const cubes = sets.map(set => {
    const parts = set.split(',').map(rawPart => {
      const part = rawPart.slice(1);
      return {
        color: part.split(' ')[1],
        count: Number(part.split(' ')[0])
      };
    });
    return {
      blue: _.sumBy(_.filter(parts, { color: 'blue'}), 'count'),
      red: _.sumBy(_.filter(parts, { color: 'red'}), 'count'),
      green: _.sumBy(_.filter(parts, { color: 'green'}), 'count'),
    };
  })
  const maxRed = _.maxBy(cubes, 'red');
  const maxGreen = _.maxBy(cubes, 'green');
  const maxBlue = _.maxBy(cubes, 'blue');

  return maxRed.red * maxGreen.green * maxBlue.blue;
}

module.exports = { runPuzzle };
