const { processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day4/input");
  let sum = 0;

  entries.map((entry) => {
    const value = getScratchScore(entry);
    sum += Number(value);
  });

  console.log(sum);
  // answer = 27059
}

function getScratchScore(entry) {
  const winningNumbers = _.compact(entry.split(': ')[1].split(' | ')[0].split(' '));
  const scratchNumbers = _.compact(entry.split(': ')[1].split(' | ')[1].split(' '));
  const matchingNumbers = _.intersection(winningNumbers, scratchNumbers);
  const points = matchingNumbers.length ? (matchingNumbers.length > 1 ? doubled(1, matchingNumbers.length) : 1) : 0;
  
  return points;
}

function doubled(value, times) {
    for (let x=1;x<times;x++) {
        value = value * 2;
    }

    return value;
}

module.exports = { runPuzzle };
