# aoc-2023

My approach to Advent of Code 2023 using JS to solve the puzzles.

[Leaderboard](https://adventofcode.com/2023/leaderboard/private/view/1030902)

## Usage

```bash
node aoc.js --day 3 --puzzle 1
```
