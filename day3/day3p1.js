const { process, processLineByLine } = require("../helpers");
const _ = require("lodash");

async function runPuzzle() {
  const entries = await processLineByLine("./day3/input");
  const partsNumbers = [];
  const symbols = _.uniq(process("./day3/input").replace(/\d/g, '').replaceAll('.', '').replaceAll('\r', '').replaceAll('\n', ''));

  for (let i=0;i<entries.length;i++) {
    const line = entries[i];
    for (let j=0;j<entries[i].length;j++) {
      const value = entries[i][j];
      const start = 0 + j;
      if (isNumber(value)) {
        const end = getEnd(j, line) + 1;
        const number = getNumber(start, end, line);
        if (hasAdjacentSymbols(start, end, i)) {
          partsNumbers.push(Number(number));
        }
        j = end;
      }
    }
  }

  function hasAdjacentSymbols(start, end, lineIndex) {
    for (let z=start-1;z<=end+1;z++) {
      if (lineIndex - 1 >= 0) {
        if (symbols.indexOf(entries[lineIndex-1][z]) >= 0) {
          return true;
        }
      }
      if (symbols.indexOf(entries[lineIndex][z]) >= 0) {
        return true;
      }
      if (lineIndex + 1 < entries.length) {
        if (symbols.indexOf(entries[lineIndex + 1][z]) >= 0) {
          return true;
        }
      }
    }

    return false;
  }

  console.log(_.sum(partsNumbers));
  // answer = 552426 WRONG
}

function isNumber(value) {
    const regex = /\d/g;
    return value.match(regex) != null;
}

function getEnd(y, line) {
  let end = y;
  if (y + 1 < line.length && isNumber(line[y+1])) {
    end = getEnd(y+1, line);
  }

  return end;
}

function getNumber(start, end, line) {
  return line.slice(start, end);
}

module.exports = { runPuzzle };
