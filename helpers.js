const { once } = require('events');
const { createReadStream, readFileSync } = require('fs');
const { createInterface } = require('readline');
const _ = require('lodash');

function process(fileName) {
  return readFileSync(fileName, "utf-8");
}

async function processLineByLine(fileName) {
  try {
    const rl = createInterface({
      input: createReadStream(fileName),
      crlfDelay: Infinity
    });
    const map = [];

    rl.on('line', (line) => {
      map.push(String(line));
    });

    await once(rl, 'close');

    return map;
  } catch (err) {
    console.error(err);
  }
}

module.exports = { process, processLineByLine };
